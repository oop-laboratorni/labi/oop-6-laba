﻿using System;
namespace ConsoleApp1

{
    public class Student
    {
        public string Name;
        protected string Surname;
        protected int Group;
        protected int Year;
        protected int Results;


        public Student(Student copyStudent)   //konstruktor kopiiuvannia
        {
            Name = copyStudent.Name;
            Surname = copyStudent.Surname;
        }


        public Student(string name, string surname) //konstruktor za parametrom 1
        {
            Name = name;
            Surname = surname;


        }
        //Realizatsiya get ta set metodiv
        public string surname { get; set; }
        public string name { get; set; }

        public string SN()
        {
            return "Iм'я: " + name + "\tПрiзвище: " + surname;
        }



        public Student(int group, int year, int results)  //konstruktor za parametrom 2
        {

            this.Group = group;
            this.Year = year;
            this.Results = results;

        }



        public void GetBestSubject() {
            int a = 10, b = 100;
            Student stud1 = new Student("Григорiй", "Сковородняк");
            Result stud11 = new Result();
            stud1.name = "Григорiй";
            stud1.surname = "Сковородняк";
            stud11.Subject = "Математика";
            if (b > a) {
                Console.WriteLine("Введiть число 100 щоб побачити найбiльший бал з усiх студентiв");
                Console.ReadLine();
                Console.WriteLine("\nНайбiльший бал:\n" + stud1.SN() + "\n|" + stud11.SBJ() + "\n|" + "Кiлькiсть балiв- " + b); //stud1.SN()



            }




        }

        public void GetWorstSubject() {
            int a = 15, b = 30;
            Student stud2 = new Student("Oлег", "Mодний");
            Student stud3 = new Student("Iнокентiй", "Бест");
            Result stud22 = new Result();
            Result stud33 = new Result();
            stud2.name = "Oлег";
            stud2.surname = "Moдний";
            stud3.name = "Iнокентiй";
            stud3.surname = "Бест";
            stud22.Subject = "Математика";
            stud33.Subject = "Англiйська та фiзкультура";
            if (a < b) {

                Console.WriteLine("Введiть число 0 щоб побачити найгiршi бали");
                Console.ReadLine();
                Console.WriteLine("\nНайгiрший бал:\n" + "1." + stud2.SN() + "\t\n"  + stud22.SBJ() + "\nКiлькiсть балiв: " + a);
                Console.WriteLine("\n2." + stud3.SN() + "\t\n"  + stud33.SBJ() + "\nКiлькiсть балiв :" + b);
            }


        }




        public void GetAveragePoints() {

            double a, b, c;


            double result;
            Console.WriteLine("Введiть оцiнку 1 студента");
            a = double.Parse(Console.ReadLine());
            Console.WriteLine("Введiть oцiнку 2 студента");
            b = double.Parse(Console.ReadLine());
            Console.WriteLine("Введiть оцiнку 3 студента");
            c = double.Parse(Console.ReadLine());
            result = (a + b + c) / 2;
            Console.WriteLine("Середнє арефметичне =" + result);


        }




    }

    class Program
    {


        static void PrintStudent() {



            Student studi1 = new Student("Aнгелiна", "Вернадська");
            Student studi2 = new Student(studi1);
            Student studi3 = new Student("Iларiон", "Православний");
            Student studi4 = new Student("Дмитро", "Вишневецький");
            Student studi5 = new Student("Iрина", "Бенедiктова");

            studi1.name = "Ангелiна";
            studi1.surname = "Вернадська";
            studi2.name = "Лаврентiй";
            studi2.surname = "Преподобний";
            studi3.name = "Iларiон";
            studi3.surname = "Православний";
            studi4.name = "Дмитро";
            studi4.surname = "Вишневецький";
            studi5.name = "Iрина";
            studi5.surname = "Бенедiктова";

            Console.WriteLine("Список студентiв якi не здали сесiю:" + "\n-" + studi1.SN() + "\n-" + studi2.SN() + "\n-" + studi3.SN() + "\n-" + studi4.SN() + "\n-" + studi5.SN());


        }


        static void PrintStudents() {

            int n = 1;
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Список студентiв якi здали сесiю:\n");
                Student student1 = new Student("Григорiй", "Сковородняк");
                Student student2 = new Student(student1);
                Student student3 = new Student("Iнокентiй", "Бест");


                student1.name = "Григорiй";
                student1.surname = "Сковородняк";
                student2.name = "Олег";
                student2.surname = "Модний";

                student3.name = "Iнокентiй";
                student3.surname = "Бест";

                Console.WriteLine(student1.SN() + "--- Задовiльно");
                Console.WriteLine(student2.SN() + "--- Незaдoвiльно");

                Console.WriteLine(student3.SN() + "--- Незадовiльно");

            }

        }



        static void SortStudentsByPoints() {

            int[] k = new int[] { 15, 30, 100 };


            for (int i = 0; i < k.Length; i++)
            {


                Console.WriteLine("\n\nСортування За  середнiм балом:\n{0}", k[i]);

            }



        }




        



        static void Main(string[] args)

        {
            do
            {

                Console.WriteLine("\nМЕНЮ");
                Console.WriteLine("\n0.Вихiд");
                Console.WriteLine("1. Показати сeреднє арефметичне");
                Console.WriteLine("2. Вивести найбiльший i нaймeншi бaли студентiв");
                Console.WriteLine("3.Вивести список студентiв якi здали сесiю(printstudents)");
                Console.WriteLine("4.Показати список студентiв  якi не здали сесiю(printstudent)");
                Console.WriteLine("5.Сортування за середнiм балом");
                Console.ReadLine();
                Console.BackgroundColor = ConsoleColor.Blue;
                int menu = int.Parse(Console.ReadLine());
                    switch (menu)
                {

                    case 0:
                        Console.WriteLine("Бувайте! Гарного дня :)");
                        return;
                        break;

                    case 1:
                       Student stud = new Student("","") ;  
                        stud.GetAveragePoints();
                        break;

                    case 2:
                        Console.WriteLine("\nНайбiльший бал студента:\n");
                        Student stu = new Student("", "");
                        stu.GetBestSubject();

                        Console.WriteLine("\nНайменший бал студента:\n");
                        stu.GetWorstSubject();
                        break;



                    case 3:
                        
                        PrintStudents();
                        break;

                    case 4:
                        PrintStudent();
                        break;

                    case 5:

                        SortStudentsByPoints();
                        break;

                
                }





            } while (true);



        }




    }



}
    


    






























/////OOP 4 LABARATORNA\\\\\\\\
//namespace ConsoleApp1
//{
//    class Program
//    {
//        static void Main(string[] args)

//        {
//            Random rnd = new Random();
//            int x = rnd.Next();


//            Console.WriteLine("Лабораторна робота номер 4");
//            Console.WriteLine("Виконав: Янович А.В.");
//            Console.WriteLine("Завдaння 1 ");
//            Console.WriteLine("-------------");

//            int n;
//            bool f;
//            do {
//                Console.Write("Введіть кількість елементів масиву:");
//                f = int.TryParse(Console.ReadLine(), out n);
//                if (f == false)
//                    Console.WriteLine("Помилка!!! Значення введено некоректно");

//            } while (!f);
//            int[] arr = new int[n];

//            for(int i=0;i<arr.Length; i++)
//            {
//                do
//                {
//                    Console.Write("arr[{0}]=", i);
//                    f = int.TryParse(Console.ReadLine(), out arr[i]);
//                    if (f == false)
//                        Console.WriteLine("Помилка!!! Значення введено некоректно");


//                } while (!f);

//            }
//            int sum = int.MinValue;
//            int index = 0;


//            for (int i = 0; i < arr.Length - 1; i++)
//            {
//                if (arr[i] + arr[i + 1] > sum)
//                {

//                    sum = arr[i] + arr[i + 1];
//                    index=i;
//                }
//            }

//            Console.WriteLine("Максимальна сума ={0}\nнайбільші індекси ={1},{2}", sum, index, index + 1);


//         Console.WriteLine("\nZavdannia 2\nSortuvannia elementiv k:");
//            int[] nums = new int[] {8,5,3,1 };
//            Array.Reverse(nums);

//                Console.WriteLine("M={0},{1},{2}", index + 1, nums);


//        }




//    }
//}
